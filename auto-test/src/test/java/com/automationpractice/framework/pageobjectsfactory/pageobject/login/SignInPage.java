package com.automationpractice.framework.pageobjectsfactory.pageobject.login;

import com.automationpractice.framework.pageobjectsfactory.BasePageObject;
import com.automationpractice.framework.pageobjectsfactory.componentobject.register.RegisterComponentObject;
import com.automationpractice.framework.pageobjectsfactory.pageobject.homepage.HomePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePageObject {

    @FindBy(id = "email")
    WebElement emailInput;
    @FindBy(id = "email_create")
    WebElement newEmailInput;
    @FindBy(id = "passwd")
    WebElement passwordInput;
    @FindBy(id = "SubmitLogin")
    WebElement submitLogin;
    @FindBy(id = "SubmitCreate")
    WebElement submitCreate;

    public final String EMAIL = getRandomEmail();

    private void setLoginEmail(String email){
        emailInput.sendKeys(email);
    }

    private void setLoginPassword(String password){
        passwordInput.sendKeys(password);
    }

    private void setRegisterEmail(String email){
        newEmailInput.sendKeys(email);
    }

    public HomePage logIlnAs(String email, String password){
        setLoginEmail(email);
        setLoginPassword(password);
        submitLogin.click();
        return new HomePage();
    }

    public RegisterComponentObject registerAs(String email){
        setRegisterEmail(email);
        submitCreate.click();
        return new RegisterComponentObject();
    }



}
