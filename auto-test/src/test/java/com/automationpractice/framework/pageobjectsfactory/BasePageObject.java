package com.automationpractice.framework.pageobjectsfactory;

import com.automationpractice.framework.common.wait.Wait;
import com.automationpractice.framework.utilities.logger.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.automationpractice.framework.utilities.driverconfig.DriverHandler.getDriver;

public class BasePageObject {

    protected WebDriver driver;
    public WebDriverWait waitFor;
    public final Wait wait;
    public Actions actions;
    protected int timeOut = 15;
    private final String CHAR_LIST = "abcdefghijklmnoprstuwxyzABCDEFGHIJKLMNOPRSTUWXYZ";

    public BasePageObject(){
        this.driver = getDriver();
        this.wait = new Wait(driver);
        this.actions = new Actions(driver);
        this.waitFor = new WebDriverWait(driver,timeOut);
        PageFactory.initElements(driver,this);
    }

    protected boolean isElementOnPage(By by) {
        wait.changeImplicitWait(500, TimeUnit.MILLISECONDS);
        try {
            return driver.findElements(by).size() > 0;
        } finally {
            wait.restoreDefaultImplicitWait();
        }
    }

    protected boolean isElementOnPage(WebElement element) {
        wait.changeImplicitWait(500, TimeUnit.MILLISECONDS);
        boolean isElementOnPage = true;
        try {
            // Get location on WebElement is rising exception when element is not present
            element.getLocation();
        } catch (WebDriverException ex) {
            isElementOnPage = false;
        } finally {
            wait.restoreDefaultImplicitWait();
        }
        return isElementOnPage;
    }

    protected boolean isElementEnabled(WebElement element) {
        return !"true".equals(element.getAttribute("disabled"));
    }


    protected boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            Log.error(e.getMessage());
            return false;
        }
    }

    protected int getNumOfElementOnPage(By cssSelectorBy) {
        wait.changeImplicitWait(500, TimeUnit.MILLISECONDS);
        int numElementOnPage;
        try {
            numElementOnPage = driver.findElements(cssSelectorBy).size();
        } catch (WebDriverException ex) {
            numElementOnPage = 0;
        } finally {
            wait.restoreDefaultImplicitWait();
        }
        return numElementOnPage;
    }

    protected void waitAndClick(WebElement element) {
        wait.forElementClickable(element).click();
    }

    public void refreshPage() {
        try {
            driver.navigate().refresh();
            Log.info("refreshPage" + "page refreshed");
        } catch (TimeoutException e) {
            Log.warn("page loaded for more than 30 seconds after click");
        }
    }

    public static String getTimeStamp() {
        Date time = new Date();
        long timeCurrent = time.getTime();
        return String.valueOf(timeCurrent);
    }

    public String getRandomDigits(int length) {
        String timeStamp = getTimeStamp();
        int timeStampLenght = timeStamp.length();
        int timeStampCut = timeStampLenght - length;
        return timeStamp.substring(timeStampCut);
    }

    public String getRandomString(int length){
        List<Character> temp = CHAR_LIST.chars()
                .mapToObj(i -> (char)i)
                .collect(Collectors.toList());
        Collections.shuffle(temp,new SecureRandom());
        return temp.stream()
                .map(Object::toString)
                .limit(length)
                .collect(Collectors.joining());
    }

    public void waitForPageToLoad(){
        wait.waitForPageLoad();
    }
    protected void hover(WebElement element) {
        actions.moveToElement(element).perform();
    }

    public String getRandomEmail(){
        return "random-" + UUID.randomUUID().toString() + "@example.com";
    }

}
