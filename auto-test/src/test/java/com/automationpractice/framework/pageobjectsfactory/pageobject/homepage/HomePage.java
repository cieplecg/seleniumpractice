package com.automationpractice.framework.pageobjectsfactory.pageobject.homepage;

import com.automationpractice.framework.pageobjectsfactory.BasePageObject;
import com.automationpractice.framework.pageobjectsfactory.pageobject.login.SignInPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePageObject {

    @FindBy(css = ".sf-menu")
    WebElement navigationBar;

    @FindBy(css = ".login")
    WebElement signInButton;

    @FindBy(css = ".logout")
    WebElement logoutButton;

    public boolean isAt(){
        return isElementOnPage(navigationBar);
    }

    public boolean isLoggedOut(){
        return isElementOnPage(signInButton);
    }

    public SignInPage openLoginPage(){
        waitAndClick(signInButton);
        return new SignInPage();
    }

    public void logOut(){
        logoutButton.click();
    }
}
