package com.automationpractice.framework.common.templates.core;

import org.testng.annotations.*;

import static com.automationpractice.framework.utilities.driverconfig.DriverHandler.*;

public abstract class CoreTestTemplate {

    //Initialize the driver factory
    @BeforeSuite(alwaysRun = true)
    public void initDriverFactory(){
        instantiateDriverObject();
    }

    //Go to first page declared under the appropriate Template (go to /common/templates)
    @BeforeClass(alwaysRun = true)
    public void initTestContext(){
        loadFirstPage();
    }

    //Remove cookies of active driver
    @AfterClass(alwaysRun = true)
    public void removeCookies(){
        clearDriverCookies();
    }

    //Clean up the driver factory
    @AfterSuite(alwaysRun = true)
    public void purgeDriverFactory(){
        closeDriverObjects();
    }

    protected abstract void loadFirstPage();
}
