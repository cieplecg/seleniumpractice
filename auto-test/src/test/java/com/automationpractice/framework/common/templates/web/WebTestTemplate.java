package com.automationpractice.framework.common.templates.web;

import com.automationpractice.framework.common.templates.core.CoreTestTemplate;

import static com.automationpractice.framework.common.Constants.ENV_URL;
import static com.automationpractice.framework.utilities.driverconfig.DriverHandler.getDriver;

public class WebTestTemplate extends CoreTestTemplate {

    @Override
    protected void loadFirstPage(){
        getDriver().navigate().to(ENV_URL);
    }
}
