package com.automationpractice.framework.testcases.auth;

import com.automationpractice.framework.common.templates.web.WebTestTemplate;
import com.automationpractice.framework.pageobjectsfactory.componentobject.register.RegisterComponentObject;
import com.automationpractice.framework.pageobjectsfactory.componentobject.register.State;
import com.automationpractice.framework.pageobjectsfactory.pageobject.homepage.HomePage;
import com.automationpractice.framework.pageobjectsfactory.pageobject.login.SignInPage;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test(testName = "LoginTests")
public class LoginTests extends WebTestTemplate {

    //Pages
    private HomePage homePage;
    private SignInPage signInPage;
    private RegisterComponentObject registerComponentObject;

    @Test(description = "Navigate to Home Page")
    public void tc01_navigatetoHomePage(){
        homePage = new HomePage();
        Assert.assertTrue(homePage.isAt(), "Home Page has not been displayed");
    }

    @Test(description = "Open Signup Page")
    public void tc02_openSignUpPage(){
        signInPage = homePage.openLoginPage();
        Assert.assertTrue(homePage.isLoggedOut(), "User is logged in by default");
    }

    @Test(description = "Enter email address and click submit")
    public void tc03_registerAs(){
        registerComponentObject = signInPage.registerAs(signInPage.EMAIL);
        Assert.assertTrue(registerComponentObject.isRegisterComponentDisplayed(), "Register component has not been displayed");

    }

    @Test(description = "Fill in the registration page")
    public void tc04_fillInRegistrationPage(){
        registerComponentObject
                .setCustomerTitle(registerComponentObject.TITLE)
                .setCustomerFirstName(registerComponentObject.FIRST_NAME)
                .setCustomerLastName(registerComponentObject.LAST_NAME)
                .setCustomerPassword(registerComponentObject.PASSWORD)
                .setBirthDate(13,11,1994)
                .setSubscriptionAlert(true)
                .setOptinCheckbox(false)
                .setCompanyAddressInput(registerComponentObject.COMPANY)
                .setAddress1CustomerInput(registerComponentObject.ADDRESS)
                .setAddress2CustomerInput(registerComponentObject.ADDRESS)
                .setCityCustomerInput(registerComponentObject.CITY)
                .setStateCustomerInput(State.TEXAS.id)
                .setPostalCustomerInput(registerComponentObject.POSTALCODE)
                .setOtherCustomerInput(registerComponentObject.OTHER)
                .setHomePhoneCustomerInput(registerComponentObject.HOME_PHONE)
                .setMobilePhoneCustomerInput(registerComponentObject.MOBILE_PHONE);
    }

    @Test(description = "Submit the registration page")
    public void tc05_submitRegistrationPage(){
        registerComponentObject.submitRegisterForm();
        Assert.assertFalse(registerComponentObject.isErrorOccured(""), "User has not been registered properly");
    }

    @Test(description = "Log out")
    public void tc06_logout(){
        homePage.logOut();
        Assert.assertTrue(homePage.isLoggedOut(), "User is still logged in");
    }
    @Test(description = "User can log in")
    public void tc07_logInPreviouslyRegisteredUser(){
        homePage.openLoginPage();
        signInPage.logIlnAs(signInPage.EMAIL,registerComponentObject.PASSWORD);
        Assert.assertTrue(!homePage.isLoggedOut(), "User is logged out");
    }

}
