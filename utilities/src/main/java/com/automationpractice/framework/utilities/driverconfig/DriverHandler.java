package com.automationpractice.framework.utilities.driverconfig;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DriverHandler {

    private static List<DriverFactory> webDriverThreadPool = Collections.synchronizedList(new ArrayList<DriverFactory>());
    private static ThreadLocal<DriverFactory> driverFactory;
    
    public static void instantiateDriverObject() {
        // Initialize webdriver
        if (driverFactory == null) {
            driverFactory = new ThreadLocal<DriverFactory>() {
                @Override
                protected DriverFactory initialValue() {
                    DriverFactory driverFactory = new DriverFactory();
                    webDriverThreadPool.add(driverFactory);
                    return driverFactory;
                }
            };
        }
    }

    public static WebDriver getDriver() {
        return driverFactory.get().getDriver();
    }

    public static void closeDriverObjects() {
        for (DriverFactory driverFactory : webDriverThreadPool) {
            driverFactory.quitDriver();
        }

    }

    public static void clearDriverCookies(){
        getDriver().manage().deleteAllCookies();
    }
}