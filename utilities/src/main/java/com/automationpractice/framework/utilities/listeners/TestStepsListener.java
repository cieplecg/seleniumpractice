package com.automationpractice.framework.utilities.listeners;

import com.automationpractice.framework.utilities.logger.Log;
import com.aventstack.extentreports.ExtentTest;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.BeforeClass;

import static com.automationpractice.framework.utilities.screenshooter.ScreenshotProvider.takeScreenshot;
import static com.automationpractice.framework.utilities.reporter.ReportManager.getParentTest;
import static com.automationpractice.framework.utilities.reporter.ReportManager.getReporter;
import static com.automationpractice.framework.utilities.reporter.ReportManager.getTest;

public class TestStepsListener implements ITestListener {

    public synchronized void onStart(ITestContext testContext) {
        ExtentTest parent = getReporter().createTest(testContext.getName());
        getParentTest().set(parent);
        Log.startTestCase(testContext.getName());
        Log.info("Started testing on: " + testContext.getStartDate().toString());
    }

    public synchronized void onFinish(ITestContext testContext) {
        getReporter().flush();
        Log.endTestCase();
    }

    public synchronized void onTestStart(ITestResult tr) {
        ExtentTest child = getParentTest().get().createNode(tr.getMethod().getDescription());
        getTest().set(child);
        Log.info(tr.getMethod().getMethodName() + " started.");
    }

    public synchronized void onTestFailure(ITestResult tr) {

        Log.error("Test failure!");
        takeScreenshot(tr);
    }

    public synchronized void onTestSkipped(ITestResult tr) {

        getTest().get().skip(tr.getThrowable());
        Log.warn("Test skipped!");
    }

    public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult tr) {
    }

    public synchronized void onTestSuccess(ITestResult tr) {
        Log.info("Test success!");
    }
}
