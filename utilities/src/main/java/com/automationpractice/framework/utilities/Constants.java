package com.automationpractice.framework.utilities;

import static com.automationpractice.framework.utilities.propertieshandler.PathHandler.getPath;

public class Constants {
    public static final String TEST_OUTPUT = getPath("output.directory");
    public static final String TEST_SETUP = getPath("setup.directory");
    public static final String TEST_SCREENSHOTS = getPath("output.directory.screenshots");
}
