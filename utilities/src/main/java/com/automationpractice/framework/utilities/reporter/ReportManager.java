package com.automationpractice.framework.utilities.reporter;

import com.automationpractice.framework.utilities.logger.Log;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import static com.automationpractice.framework.utilities.Constants.TEST_OUTPUT;

public class ReportManager {

    private static ExtentReports extent;
    private static ThreadLocal<ExtentTest> parentTest;
    private static ThreadLocal<ExtentTest> test;

    public synchronized static ExtentReports getReporter() {
        if (extent == null)
            createInstance();

        return extent;
    }

    private synchronized static void createInstance() {

        parentTest = new ThreadLocal<>();
        test = new ThreadLocal<>();

        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(TEST_OUTPUT + "run-report.html");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle("AutomationRegression");
        htmlReporter.config().setEncoding("UTF-8");
        htmlReporter.config().setReportName("AutomationRegression");

        extent = new ExtentReports();
        extent.setSystemInfo("OS", System.getProperty("os.name"));
        extent.setSystemInfo("BROWSER", System.getProperty("browser"));
        extent.setSystemInfo("ARCH", System.getProperty("os.arch"));
        extent.setReportUsesManualConfiguration(true);
        extent.attachReporter(htmlReporter);
        Log.info("ReportManager was created.");
    }

    public synchronized static ThreadLocal<ExtentTest> getParentTest() {
        return parentTest;
    }

    public synchronized static ThreadLocal<ExtentTest> getTest() {
        return test;
    }
}